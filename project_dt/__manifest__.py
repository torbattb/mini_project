# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Mini Project',
    'version': '1.1',
    'website': '',
    'category': 'Project',
    'price': 250.00,
    'currency': 'USD',
    'sequence': 1,
    'summary': 'Organize and schedule your mini projects. Progress of Tasks, pre-configure stages etc... Mini project. Project mini. Little project. Easy project. Small project. Project simultaneous. Double project. Project double use.',
    'license': 'AGPL-3',
    'author': 'Tb25',
    'email': 'torbatj79@gmail.com',
    'depends': [
        'base',
        'base_setup',
        'project',
        'hr',
        #'crm_project',
    ],
    'description': "If you need same module of Project Management as same when installed module of Project Management base, you can use this module",
    'data': [
        'security/ir.model.access.csv',
        'views/project_views.xml',
        'data/project_data.xml',
        #'data/project_demo.xml',
    ],
    #'qweb': ['static/src/xml/project.xml'],
    'demo': ['data/project_demo.xml'],
    #'test': [],
    'images': [
        'static/description/banner.png'
    ],
    
    'installable': True,
    'auto_install': False,
    'application': True,
}
